# Chrome Extension 
---
## Name : 

## Project Structure

+ **CHANGELOG**

     file containing a chronologically ordered list of all notable changes made to the project during every 
     new release.

+ **CONTRIBUTING.md**

     contains contribution guidelines for all those who are looking forward to contribute to the project in
     the form of bugfixes, patches, enchancements etc.

+ **LICENSE**

     file containing legal instructions that govern the usage and redistribtuion of the source code as well
     as the finished product.  

+ **README.md**

     this is the file that your reading right now ... :) 

+ **src**

     this folder contains all the required source code and other dependent resources that are required to deploy the extension.

 + **src/support** 

        folder that holds all the advertisement code.

 + **src/support/support.js** 

        This is the javascript file that gets injected into every web page and inserts the advertisement code.
     
 + **src/support/background.js**

        This js file comunicates with the server to keep refereshing the black and whitelists that decide whether an ad will         be shown or not.
     
 + **src/support/ext.js**

        contains the zone id which is used in the analytics to track impressions per extension.